# features:

- Có thể xem số liệu theo thời gian

# layouts:

- <strong>Người quản lý:</strong>
  - Dựa vào bảng số liệu và biểu đồ, họ có thể xem được các team đã sale được bao nhiêu, mục tiêu đề ra và % so với mục tiêu đề ra.
  - Đánh giá được team nào đang sale tốt nhất thông qua biểu đồ
- <strong>Team lead:</strong>
  - Thành viên trong team có thể biết được team của họ sale được bao nhiêu, % so với mục tiêu và thành viên nào đứng top đầu
- <strong>Người sale:</strong>
  - Có thể xem được performance của mình tới đâu, còn bao nhiêu % nữa là đủ target.
