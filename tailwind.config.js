module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],

  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},

    container: {
      center: true,
      padding: {
        DEFAULT: "1rem",
        sm: "2rem",
        lg: "4rem",
        xl: "5rem",
        "2xl": "6rem",
      },
    },
    // fontSize: {
    //   sm: ["14px", "20px"],
    //   base: ["16px", "24px"],
    //   lg: ["20px", "28px"],
    //   xl: ["24px", "32px"],
    // },
    screens: {
      sm: "480px",
      md: "768px",
      lg: "976px",
      xl: "1024px",
      xxl: "1440px",
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
