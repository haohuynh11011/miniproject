import { create } from "apisauce";
const api = create({ baseURL: "https://api.itbclub.com/api" });

async function getListProvince() {
  const response = await api.head("/zipCodes");
  console.log(response);
}
export { getListProvince };
