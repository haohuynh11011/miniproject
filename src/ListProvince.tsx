import React, { useContext, useEffect } from "react";
import ProvinceStore from "./mobx/provinceStore";
import { observer } from "mobx-react-lite";
function ListProvince() {
  const provinceStore = useContext(ProvinceStore);
  const { listProvince, fetchListProvince } = provinceStore;
  useEffect(() => {
    fetchListProvince();
  }, []);
  return (
    <div>
      {listProvince.map((province) => (
        <tr>{province.Position}</tr>
      ))}
    </div>
  );
}

export default observer(ListProvince);
