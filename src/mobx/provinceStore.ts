import { observable, action, computed, reaction, makeObservable } from "mobx"
import { createContext } from "react"
import { getListProvince } from "../apis"

export interface Province {
    Id: number;
    Position:string
}
class ProvinceStore{
    listProvince:Province[]=[]
    constructor() {
        makeObservable(this, {
            listProvince: observable,
            fetchListProvince: action,
        })
    }
    fetchListProvince=async()=>{
            try {
                const data=await getListProvince()
                this.listProvince=data
            } catch (error) {
                console.log(error,"error")
            }
    }

}
export default createContext(new ProvinceStore())