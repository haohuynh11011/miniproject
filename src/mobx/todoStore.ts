import { observable, action, computed, reaction, makeObservable } from "mobx"
import { createContext } from "react"

export interface Todo {
    id?: number;
    title: string;
    completed: boolean;
}
class TodoStore {
    todos: Todo[] = [
        { id: 1, title: "Item #1", completed: false },
        { id: 2, title: "Item #2", completed: false },
        { id: 3, title: "Item #3", completed: false },
        { id: 4, title: "Item #4", completed: false },
        { id: 5, title: "Item #5", completed: true },
        { id: 6, title: "Item #6", completed: false },
    ]

    constructor() {
        reaction(() => this.todos, _ => console.log(this.todos.length))
        makeObservable(this, {
            todos: observable,
            toggleTodo: action,
            removeTodo:action,
        })
    }

    
    toggleTodo = (id: number) => {
        
        this.todos = this.todos.map(todo => {
          if (todo.id === id) {
            return {
              ...todo,
              completed: !todo.completed,
            }
          }
          return todo
        })
      }
      removeTodo=(id:number)=>{
        const todoId = this.todos.findIndex((todo) => todo.id === id);
        if(todoId>-1){
          return this.todos.splice(todoId,1)
        }
      }
    @computed get info() {
        return {
          total: this.todos.length,
          completed: this.todos.filter(todo => todo.completed).length,
          notCompleted: this.todos.filter(todo => !todo.completed).length,
        }
      }
}
 
export default createContext(new TodoStore())