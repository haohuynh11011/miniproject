import { useEffect, useState } from "react";
import { CTable, TTableColumn } from ".";
import { SaleTarget } from "../../type";

const myColDefinition: TTableColumn[] = [
  {
    key: "sale",
    colLabel: "Sale",
  },
  {
    key: "target",
    colLabel: "Target",
  },
  {
    key: "percentageOfTarget",
    colLabel: "% of target",
  },
  {
    key: "different",
    colLabel: "Different",
  },
];

type TTableCard = {
  label: string;
  data: SaleTarget[];
};
function TableRoom(props: TTableCard) {
  return (
    <div className="md:w-full m-5   shadow-md rounded ">
      <div className="m-3   align">
        <label className="text-lg font-bold	">{props.label}</label>
      </div>
      <div className="   mt-3  ">
        <CTable columnsSchema={myColDefinition} data={props.data} />
      </div>
    </div>
  );
}

export { TableRoom };
