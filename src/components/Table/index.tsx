import { useEffect, useState } from "react";

export type TTableColumn = {
  key: string;
  colLabel: string;
};

type TTableProps<T> = {
  columnsSchema: TTableColumn[];
  data: T[];
};
type DynamicObject = {
  [key: string]: unknown;
};
const CTable: <T>(props: TTableProps<T>) => React.ReactElement<TTableProps<T>> =
  (props) => {
    const [tableColsObj, setTableColsObj] = useState<DynamicObject>();

    useEffect(() => {
      const colsObj: DynamicObject = {};
      props.columnsSchema.forEach((colDef) => {
        colsObj[colDef.key] = colDef.key;
      });
      setTableColsObj(colsObj);
    }, []);

    /**
     * {
     *  email:email,
     * password:password
     * }
     */

    return (
      <table className="table-auto w-full">
        <thead className="wd:w-full bg-gray-200">
          <tr>
            {props.columnsSchema.map((colDefinition) => {
              return <th className="w-1/4 p-3">{colDefinition.colLabel}</th>;
            })}
          </tr>
        </thead>
        <tbody>
          {props.data.map((row, index) => {
            const colStructure = tableColsObj ?? {};
            return (
              <tr key={index}>
                {Object.keys(colStructure).map((colKey) => {
                  // const rowData = row as unknown as DynamicObject;
                  // const cellValue = rowData[colKey] as string;
                  return <td className="w-1/4 text-center	 p-3"></td>;
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  };

export { CTable };
