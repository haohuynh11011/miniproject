import CardPerson from "./CardPerson";
const fakeCardPerson = [
  {
    rank: 1,
    image: "https://via.placeholder.com/350",
    name: "Justin Timberlake",
    sales: "123 321",
    target: "321 123",
    percent: "100%",
  },
  {
    rank: 2,
    image: "https://via.placeholder.com/350",
    name: "Justin Timberlake",
    sales: "123 321",
    target: "321 123",
    percent: "100%",
  },
  {
    rank: 3,
    image: "https://via.placeholder.com/350",
    name: "Justin Timberlake",
    sales: "123 321",
    target: "321 123",
    percent: "100%",
  },
  {
    rank: 4,
    image: "https://via.placeholder.com/350",
    name: "Justin Timberlake",
    sales: "123 321",
    target: "321 123",
    percent: "100%",
  },
  {
    rank: 5,
    image: "https://via.placeholder.com/350",
    name: "Justin Timberlake",
    sales: "123 321",
    target: "321 123",
    percent: "100%",
  },
  {
    rank: 6,
    image: "https://via.placeholder.com/350",
    name: "Justin Timberlake",
    sales: "123 321",
    target: "321 123",
    percent: "100%",
  },
  {
    rank: 7,
    image: "https://via.placeholder.com/350",
    name: "Justin Timberlake",
    sales: "123 321",
    target: "321 123",
    percent: "100%",
  },
  {
    rank: 8,
    image: "https://via.placeholder.com/350",
    name: "Justin Timberlake",
    sales: "123 321",
    target: "321 123",
    percent: "100%",
  },
  {
    rank: 9,
    image: "https://via.placeholder.com/350",
    name: "Justin Timberlake",
    sales: "123 321",
    target: "321 123",
    percent: "100%",
  },
  {
    rank: 10,
    image: "https://via.placeholder.com/350",
    name: "Justin Timberlake",
    sales: "123 321",
    target: "321 123",
    percent: "100%",
  },
];
function TeamDashboard() {
  return (
    <div className="p-2 ">
      <div className="my-5 ">
        <span className="text-base font-bold ">S. Sale team Dashboard</span>
      </div>
      <div className="my-5  flex lg:w-1/2 md:w-1/2 w-full">
        <div className="w-50 border border-gray-700 w-1/2 flex justify-center items-center">
          <span className="font-bold text-base">Select a month</span>
        </div>
        <div className="w-50 border border-gray-700 w-1/2 flex justify-center items-center">
          <span className="text-base font-bold">Total</span>
        </div>
      </div>
      <div className="flex grid grid-cols-4 gap-2   rounded ">
        <div className=" lg:col-span-1 col-span-6 py-2 justify-between lg:block sm:flex md:flex">
          <ResultData title="Sales" numberDate="155 550" />
          <ResultData title="Target" numberDate="123 321" />
          {renderChart()}
        </div>
        <div className=" lg:col-span-3 col-span-6 box h-full ml-1 flex flex-col md:flex-wrap flex-nowrap">
          {fakeCardPerson.map((cardPerson) => {
            return (
              <CardPerson
                rank={cardPerson.rank}
                image={cardPerson.image}
                name={cardPerson.name}
                sales={cardPerson.sales}
                target={cardPerson.target}
                percent={cardPerson.percent}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
}

type ResultDataPropsType = {
  title: string;
  numberDate: string;
};
const ResultData = (props: ResultDataPropsType) => {
  return (
    <>
      <div className="border-gray-400 border-b py-5 h-1/4">
        <h4 className="text-base text-center">{props.title}</h4>
        <h1 className="text-3xl text-center font-bold mt-10">
          {props.numberDate}
        </h1>
      </div>
    </>
  );
};
const renderChart = () => {
  return (
    <>
      <div className=" py-5 h-1.5/4">
        <h4 className="text-base text-center">% of Target</h4>
        <div className="circle_box flex items-center justify-center mt-10">
          <div className="circle border-yellow-400 relative rounded-full">
            <div className="absolute  right-0 top-0 mt-6 mr-4">
              <span className="text-2xl text-center font-700 mt-10">101%</span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default TeamDashboard;
