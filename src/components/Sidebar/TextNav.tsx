/* eslint-disable prettier/prettier */
interface textNavProps {
    icon: any;
    title: string;
}
function TextNav(props: textNavProps) {
    const { icon, title } = props;
    return (
        <>
            <a
                href=""
                className="py-2 px-4 flex items-center transition duration-200 hover:bg-gray-700 rounded justify-between"
            >
                <div className="flex items-center">
                    <a className="text-white">{icon}</a>
                    <span className="text-base text-white mx-1">{title}</span>
                </div>
                <a>
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-6 w-6"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                    >
                        <path
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            stroke-width="2"
                            d="M15 19l-7-7 7-7"
                        />
                    </svg>
                </a>
            </a>
        </>
    );
}

export default TextNav;
