import * as Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";

const options: Highcharts.Options = {
  chart: {
    type: "column",
  },

  title: {
    text: "Total fruit consumption, grouped by gender",
  },

  xAxis: {
    categories: ["Apples", "Oranges", "Pears", "Grapes", "Bananas"],
  },

  yAxis: {
    allowDecimals: false,
    min: 0,
    title: {
      text: "Number of fruits",
    },
  },

  plotOptions: {
    column: {
      stacking: "normal",
    },
  },

  series: [
    {
      type: "column",
      name: "John",
      data: [5, 3, 4, 7, 2],
      stack: "male",
    },
    {
      type: "column",

      name: "Joe",
      data: [3, 4, 4, 2, 5],
      stack: "male",
    },
    {
      type: "column",

      name: "Jane",
      data: [2, 5, 6, 2, 1],
      stack: "female",
    },
    {
      type: "column",

      name: "Janet",
      data: [3, 0, 4, 4, 3],
      stack: "female",
    },
  ],
};

const Chart = (props: HighchartsReact.Props) => (
  <div>
    <HighchartsReact highcharts={Highcharts} options={options} {...props} />
  </div>
);
export { Chart };
