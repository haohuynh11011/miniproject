export type SaleTarget = {
    sale: number
    target: number
    percentageOfTarget: number
    different: number
}