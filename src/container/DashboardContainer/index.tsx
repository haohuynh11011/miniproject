import { DatePicker } from "antd";
import { Chart } from "../../components/Chart";
import { TableRoom } from "../../components/Table/TableRoom";
import TeamDashboard from "../../components/TeamDashboard";
import { SaleTarget } from "../../type";
const tableData: SaleTarget[] = [
  {
    sale: 9999,
    target: 9999,
    percentageOfTarget: 33,
    different: 444,
  },
  {
    sale: 9999,
    target: 9999,
    percentageOfTarget: 33,
    different: 444,
  },
];
function DashboardContainer() {
  return (
    <>
      <div className="md:container md:mx-auto py-4 px-4 h-screen  overflow-auto 		">
        <div className="overscroll-auto	">
          <div className="flex items-center justify-center flex-col">
            <div className="flex justify-end	w-full">
              <DatePicker />
            </div>
            <TableRoom label="Phòng kinh doanh" data={tableData} />

            <TableRoom label="Phòng CS" data={tableData} />
          </div>
          <Chart />
          <TeamDashboard />
        </div>
      </div>
    </>
  );
}

export default DashboardContainer;
