import React, { useContext } from 'react';
import TodoStore from './mobx/todoStore';
import { observer } from "mobx-react-lite";
function TodoList() {
    const todoStores = useContext(TodoStore);
    const { todos, toggleTodo, info, removeTodo } = todoStores;
    return (
        <div>
            <div className="alert alert-primary">
                <div className="d-inline col-4">
                    Total items: &nbsp;
                    <span className="badge badge-info">{info.total}</span>
                </div>
                <div className="d-inline col-4">
                    Finished items: &nbsp;
                    <span className="badge badge-info">{info.completed}</span>
                </div>
                <div className="d-inline col-4">
                    Unfinished items: &nbsp;
                    <span className="badge badge-info">{info.notCompleted}</span>
                </div>
            </div>
            <div className="table_box">
                <table >
                    <tbody>
                        <tr>
                            <th>id</th>
                            <th>title</th>
                            <th>status</th>
                            <th>action</th>
                        </tr>
                        {todos.map((todo, index) => (
                            <tr key={index}>
                                <td>{todo.id}</td>
                                <td style={{ width: "200px" }}>{todo.title}</td>
                                <td>{todo.completed ? "true" : "false"}</td>
                                <td style={{ width: "150px" }}>
                                    <div className="btn_box">
                                        <button onClick={_ => toggleTodo(todo.id!)}>click</button>
                                        <button onClick={_ => removeTodo(todo.id!)}>remove</button>
                                    </div>

                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>

            </div>

        </div>
    );
}

export default observer(TodoList);