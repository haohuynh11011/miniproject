import "./index.css";
import "./App.css";
import "antd/dist/antd.css";
import { Leftbar } from "./components/Sidebar/Leftbar";
import DashboardContainer from "./container/DashboardContainer";

function App() {
  return (
    <div className="grid grid-cols-5 ">
      <div className="col-span-5 lg:col-span-1">
        <Leftbar />
      </div>
      <div className="col-span-5 lg:col-span-4">
        <DashboardContainer />
      </div>
    </div>
  );
}
export default App;
