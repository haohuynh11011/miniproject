import axios, { Method } from "axios"
interface apiRequest {
    url: string;
    method: Method;
    data?: any;
    baseUrl?: string;
    headers?: Object;
}
async function getListProvince() {
    const getListProvinceReques: apiRequest = {
        url: "https://api.itbclub.com/api/zipCodes",
        method: "GET",
        baseUrl: "https://api.itbclub.com/api",
    }
    const response = await axios.request(getListProvinceReques);
    return response.data;
}
export { getListProvince }
